# Assassin's Creed Odyssey 

### Game Details
Assassin's Creed Odyssey is set to release on October 4th, 2018. This entry in the franchise introduces players to the ability to choose your fate. Begin your story as an outcast as you embark on an epic journey to become a living legend. Explore Ancient Greece as you travel from green forests to volcanic islands and through sprawling cities. Assassin's Creed Odyssey will feature combat ranging from small alley-way brawls to fighting on the front lines of massive battles with hundreds of warriors. System Requirement information will be updated as it is released.


![ac_odyssey_wallpaper](images/ac_odyssey_wall.jpg)

### My System Requirements

| Component | Requirements |
| --- | ----------- |
| CPU | AMD Ryzen 3100 |
| RAM | 8 GB |
| OS | Windows 10 |
| GRAPHICS CARD| AMD Radeon RX 580 8GB |
| GAME SIZE | 90 GB |

> **Note** <br> 
This recommendation is for based on low level cpu and gpu. An 4 core CPU and GPU is like nvidia ***GTX 1060 3GB*** and amd ***RX 470/570 (4GB)*** or something similar.. <br>

### Recommended Display Settings 

| Items | Selected Options |
| ---- | ------ |
| Brightness | 50% | 
| Resolution Modifier | 100% |
| Display Resolution | 1920x1080 |
| VSync | OFF |
| Field of View | 100% |

### Recommended Graphic Settings 

| Items | Selected Options |
| ---- | ------ |
| Adaptive Quality | High |
| Shadows | Medium |
| Anti-Aliasing | High |
| Environment Details | High |
| Texture Details | High |
| Terrain | High |
| Clutter | Very High |
| Fog | Medium |
| Water | High |
| Screen Space Reflection | High|
| Volumetric Clouds | Medium |
| Texture Details | High |
| Character | Ultra High |
| Ambient Occlusion | Very High|
| Depth of field | Low |

### AMD Adrenaline Settings 
| Items | Selected Options |
| ---- | ------ |
| FreeSync | ON | 
| Radeon Anti-Lag | Enabled |
| Radeon Enhanced Sync | Enabled |


### Performance Results 
* Average FPS **60 - 75** 

### How to apply save file 

* **Empress** ~ 

  * Copy that Save file to `%appdata%\uplay_emu\EMPRESS\5059\storage_files`
  
  * Check the Detect option near Decryption ID    
  
  * In Encryption ID enter "c91c91c9-1c91-c91c-91c9-1c91c91c91c9"
  
  * Click Convert and delete/move the backup folder
  
  * Enjoy!!
  
    > You can just move the retail save files to empress save location in **appdata**, and go into the `ini` file in the main game folder of the empress crack called `uplay_r1_loader`  https://prnt.sc/rs93un and set the **userid** to your original uplay id which you can find is the name of the folder your saves are in in the retail. Either way works. Cheers guys.
    >
    > Make sure you put the saves inside the file called `storage_files` or it won't read them you can't just put them in the **5059** folder like in retail. I made that mistake and was beating my head about it until I realized it was a strict path lol.
  
* **CPY** and **Steam** ~ 

  You will need to copy the save data to this Assassin’s Creed Odyssey save game location – **\Program Files (x86)\Ubisoft\Ubisoft Game Launcher\savegames** (this is the folder location for this game – **\Program Files (x86)\Ubisoft\Ubisoft Game Launcher\savegames\c872388e-c9b0-456b-bdf6-a08e766678cd\5059**)

  In case, the Assassin’s Creed Odyssey save game doesn’t work, then try to start Uplay client in offline mode, to do that follow the steps given below:

  1. Launch the Uplay Client.
  2. Before login, at the bottom of the screen click on Settings.
  3. Tick mark the option of “Always start Uplay in Offline Mode”.
  4. Click back and then login with your Email ID and Password. The client will launch in offline mode, then click on Play to launch the game.

# [Guide](https://www.ign.com/wikis/assassins-creed-odyssey)

# [Cult of Kosmos - Cultist Locations and Spear Upgrades Guide](https://www.ign.com/wikis/assassins-creed-odyssey/Cult_of_Kosmos_-_Cultist_Locations_and_Spear_Upgrades_Guide)

* Eyes of Kosmos
* The Silver Vein
* Delian League
* Peloponnesian League
* Gods of the Aegean Sea
* Worshippers of the Bloodline
* Heroes of the Cult
* The Chosen One
* The Ghost of Kosmos

### [The Eyes of Kosmos Locations](https://www.shacknews.com/article/107718/the-eyes-of-kosmos-locations-assassins-creed-odyssey)

Don't let them fool you. Despite being the first group of cultists that you'll hunt down, the Eyes of Kosmos still pose quite at threat to the world. Below you'll find details about each cultist's location, as well as maps that will point you right to where you need to go to find them.

#### Elpenor 

* Unlocked By: Snake in the Grass
* Location: Snake Temple, Valley of the Snake, Phokis
* Loot: Scaled Torso

![](images/Elpenor.webp)

Elpenor is a character that players will meet through the main story of Assassin’s Creed Odyssey and one that will kick off the hunt for The Eyes of Kosmos. Players will eliminate Elpenor during the Snake in the Grass quest. If you cannot find the Scaled Torso legendary armor piece he drops, return to the cave where you killed him and loot his body.

#### Sotera 


* Unlocked By: The Serpent’s Lair
* Location: Port of Nisaia, Valley of King Lelex, Megaris
* Loot: Slithering Belt

![](images/Sotera.webp)

Sotera is easy to track down because the clue to find her is given during the main story quest, The Serpent’s Lair. She is found in the Port of Nisaia and can be found incredibly close to the water’s edge, making the approach rather easy to eliminate this target.

#### The Master

* Unlocked By: A Life’s Worth
* Location: Lavrio Silver Mine, Silver Mountain, Attika
* Loot: Viper’s Hood

![](images/Master.webp)

Probably the hardest of The Eyes of Kosmos to unlock. You must progress the main story through all of Attika, and then start knocking out the side quests. Eventually, south of Greater Athens is a quest called A Life’s Worth, which is what introduces The Master. If you don’t see it, keep doing all side quests in Attika. After the quest is complete, I had to fast travel to the nearby synchronization point, then track The Master from the Cultist menu, at which point he appeared on my map.

#### Hermippos 


* Unlocked By: Free Speech
* Location: Temple of Hephaistos, Greater Athens, Attika
* Loot: Venom Gloves

![](images/Hermippos.webp)

After pushing the main story quests through Attika, side quests will begin to pop up in the Greater Athens area. Complete Citizenship Test and Witness Him, then Free Speech. The latter is the side quest that will reveal Hermippos as an Eye of Kosmos member and provide his location.

#### Midas 


* Unlocked By: The Serpent’s Lair
* Location: Temple of Poseidon, Argos, Argolis
* Loot: Noxious Boots

![](images/Midas.webp)

You should get Midas unlocked by completing The Serpent’s Lair quest. He’s easy enough to kill but is in the open and surrounded by lots of guards. Confirming the kill could be difficult given the enemies, but a little patience and roof cheesing should help you get the job done.

#### Nyx the Shadow


* Unlocked By: Eliminating all Eyes of the Kosmos
* Location: Statue of Athena, Greater Athens, Attika
* Loot: Dagger of Kronus

![](images/Nyx.webp)

The Sage for The Eyes of the Kosmos is Nyx the Shadow. She can be found southeast of the Statue of Athena in Greater Athens, Attika. You must first eliminate all Eyes of the Kosmos to unlock her location. She travels alone, so this shouldn’t be too difficult if you are properly leveled.

Having all five legendary Snake Set armor pieces equipped will activate a perk that grants +10% Intoxicated Damage and Weakening Effect. You do not need to equip the Dagger of Kronus for receive this bonus, but that weapon alone grants +40% Damage when Attacking from Behind.

With the Eyes of Kosmos gone and the Nyx the Shadow Sage also in the ground, return to the Assassin’s Creed Odyssey walkthrough and guide to continue your journey.


### [The Silver Vein locations](https://www.shacknews.com/article/107789/the-silver-vein-locations-assassins-creed-odyssey)
While you'll be able to take down the first member of this cultist branch early on, the rest will require you to move up the ranks and progress through the story. Check out our essential tips and tricks for more help tackling everything that Assassin's Creed Odyssey throws at you.

#### Epiktetos the Forthcoming

* Unlocked By: The Serpent’s Lair
* Location: Sanctuary of Delphi, Grand Mount Parnassos, Phokis
* Loot: Agamemnon’s Gauntlets

![](images/Epiktetos.webp)

The first member of The Silver Vein that you kill will be done through a story missions, so there’s no fear that you can miss this guy or be under leveled. The moment you get the Cultists tab in your inventory, Epiktetos the Forthcoming should already be dead.

#### The Chimera

* Unlocked By: The Serpent’s Lair
* Location: Beach Encampment, Steropes Bay, Andros
* Loot: Boots of Agamemnon

![](images/Chimera.webp)

The Chimera gives mixed reports on what her level is, but she’s level 20. She should be unlocked from the moment you have the list, so once you get close to level 20 you can hunt her. She’s got a few guards with her, but a Predator Shot to the head drops her easily enough if your character is configured for a bit of Hunter damage. Don't forget to confirm the kill once the guards have been eliminated.

#### The Centaur of Euboea

* Unlocked By: The Serpent’s Lair
* Location: Abandoned Mine, Mount Dirfi, Euboea
* Loot: Agamemnon’s Waistband

![](images/Euboea.webp)

The Centaur of Euboea should be unlocked as soon as you get the list. There are guards outside of the abandoned mine and inside, so you’re probably going to have a fight on your hands. Try sneaking into the mine without conflict to make your life easier.

#### The Silver Griffin

* Unlocked By: Slaver’s Gift (Clue)
* Location: Prasonisia Island, Tavern Point, Mykonos
* Loot: Agamemnon’s Body Armor

![](images/Griffin.webp)

The clue to find The Silver Griffin is in the Silver Mine, part of the Silver Mountain region in Attika. Inside this mine there will be a legendary chest, and the Slaver’s Gift clue is inside. This will unveil both the identity of The Silver Griffin, and his location.

#### Machaon the Feared

* Unlocked By: Dead Man’s Note (Clue)
* Location: Patrai Military Shipyard, Mount Panachaikos, Achaia
* Loot: Agamemnon’s Helmet

![](images/Feared.webp)

The Dead Man’s Note clue to finding Machaon the Feared is in Shipwreck Cove, part of Scavenger’s Coast in Achaia. It can be found sitting on a rock. Loot the dead body to unveil the location and identity of Machaon the Feared, who is hanging out just along the coastline from where you are when you find the clue. If you are under leveled, park your ship next to the shore and use Predator Shot to slowly kill him. Might take 100 arrows, but he'll never attack you if you stay on your ship.

#### Polemon the Wise

* Unlocked By: Eliminating all Silver Vein members
* Location: Teichos of Herakles, Erymanthos Peaks, Achaia
* Loot: Elysian Axe

![](images/Polemon.webp)

Polemon the Wise is the Sage for The Silver Griffin wing of the Cult of Kosmos. He is only revealed after you eliminate the other five members, each dropping a clue. However, Polemon the Wise can be killed while you’re extremely under leveled. Stand on top of the building he’s in and use your bow to whittle his health away slowly. Predator Shot and Ghost Arrows of Artemis will do the trick, and at level 33 I snagged over 43,000 XP by killing him and completing The Silver Vein quest.

### [Delian League](https://www.shacknews.com/article/107882/delian-league-locations-assassins-creed-odyssey)

We've broken down the locations of each of the Delian League cultists below. Available soon after you kill Elpenor, these cultists start off at a somewhat higher level than some of the other branches. Make sure you're prepared for the battle before using the info below to find and kill each cultist.

#### Brison

* Unlocked By: The Serpent’s Lair
* Location: Salamis Marble Quarry, Salamis Island, Attika
* Loot: Athenian War Hero Gauntlets

![](images/Brison.webp)


Brison is handed to you after playing through The Serpent’s Lair story mission, so he can be approached relatively early. He will not drop clues to any other members of the Delian League, which is a shame but not a big deal.

#### Podarkes the Cruel

* Unlocked By: Not Required
* Location: Leader House, Tavern Point, Silver Islands (Mykonos)
* Loot: Athenian War Hero Boots

![](images/Podarkes.webp)

I had no idea that Podarkes the Cruel was a member of the Cult of Kosmos or Delian League. There were several quests on the island of Mykonos, but I opted to experiment with just killing the leader to see if it would clear them all. It did, and it turns out that Podarkes was a member of the Delian League. Just go kill this guy when you’re ready, don’t bother trying to unveil him.

#### Rhexenor the Hand

* Unlocked By: A-Musing Tale (Quest)
* Location: Weapon Storage, Greater Athens, Attika
* Loot: Athenian War Hero Belt

![](images/Rhexenor.webp)

Rhexenor the Hand is discovered during a main quest line, so there’s no harm in just playing through to unveil him. You should be leveled with him, but I still opted to load up on Hunter damage and shoot him from across the street. Two arrows did it and I had rid myself of one more member of the Delian League.

#### Iobates the Stoic

* Unlocked By: Killing Aigle the Great Cat (Mercenary)
* Location: Ancient Stronghold, Sky Fall Lakes, Hephaistos Islands (Lemnos)
* Loot: Athenian War Hero Armor

![](images/Iobates.webp)

I was very under leveled (level 41) when I opted to take on Iobates the Stoic, so I loaded up on gear that bumped my Hunter damage and used the bow and arrow to take him out at range. He fled the fort, making my life easier and allowing me to eliminate him away from the bulk of his forces. If this isn’t an option for you, level a bit and come back for him.

#### Kodros the Bull

* Unlocked By: Killing Titos of Athens (Arena)
* Location: Leader House, Lover’s Bay, Petrified Islands (Lesbos)
* Loot: Athenian War Hero Helmet


![](images/Kodros.webp)

The location and identity of Kodros the Bull is uncovered by killing Titos of Athens, a level 43 fighter in the Arena. He dropped a clue called Job Offer, which is what provided the information. Kodros the Bull was a level 50 enemy while I was only level 41, but a build focused on Hunter damage and a safe position to use my bow made quick work of him.

#### Kleon the Everyman

* Unlocked By: Doing Time (Quest)
* Location: Battleground of Makedonia, Roots of an Empire, Makedonia
* Loot: Paris’s Bow

![](images/Kleon.webp)

Kleon the Everyman is discovered through the main quests in Assassin’s Creed Odyssey, so you can’t really miss him. You also can’t get to him early. You must wait for the story to make him your target. By the time you get to him all other members of the Delian League should be eliminated.

### [Peloponnesian League](https://www.shacknews.com/article/107914/peloponnesian-league-locations-assassins-creed-odyssey)


### [Gods of the Aegean Sea](https://www.shacknews.com/article/107796/gods-of-the-aegean-sea-locations-assassins-creed-odyssey)

### [Worshippers of the Bloodline](https://www.shacknews.com/article/117905/worshippers-of-the-bloodline-cultist-locations-assassins-creed-odyssey)

### [Heroes of the Cult](https://www.gamepressure.com/assassins-creed-odyssey/heroes-of-the-cult/z2b718)

### [The Chosen One](https://www.ign.com/wikis/assassins-creed-odyssey/Cult_of_Kosmos_-_Cultist_Locations_and_Spear_Upgrades_Guide#The_Ghost_of_Kosmos)

### [The Ghost of Kosmos](https://www.ign.com/wikis/assassins-creed-odyssey/Cult_of_Kosmos_-_Cultist_Locations_and_Spear_Upgrades_Guide#The_Ghost_of_Kosmos)

**Documentation By** <br>
[Farhan Sadik](https://gitlab.com/farhansadik)

**Source** <br>
<https://www.reddit.com/r/CrackWatch/comments/ftkvkj/ac_odyssey_proper_way_to_convert_a_cpy_save_to_a/fm8ci36/> <br>
<https://gametransfers.com/assassins-creed-odyssey-save-game-download/> <br>

